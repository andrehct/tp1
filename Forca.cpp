#include <iostream>
#include <stdlib.h>
#include <vector>

using namespace std;

int i, erro=7;
char *aux;
vector <char> le;

void exibe (int tam, string d);
bool compara (string a, string b, int tam);
bool confere(char l);
void converter (string &s);

int main (){

	string word,dica,letra;
	int x;
	bool b;
	aux = (char *) malloc(sizeof(char)); 

	exibe(0, " "); //pode passar qualquer int como parametro, pois � apenas a inicializacao nessa linha
	
	system ("color 3F");
	cout << "\n\nInsira a palavra : ";
	getline(cin, word);
	
	cout << "\nInsira a dica: ";
	getline(cin, dica);
	
	int tam = word.size();
	
	for (i=0; i< tam ; i++)
	{
		aux[i] = '_';
	}
	
	converter(word); // converte a palavra para letras maiusculas e troca espacos por '-'
	
	system ("cls");

	do
	{
		exibe(tam, dica);
    	cout << "\n\n\tInsira uma letra : ";
		getline(cin, letra); 
		letra = toupper(letra[0]);
		b = confere(letra[0]);
		if(b)
		{
			cout << "\n\tLetra ja utilizada" << endl;
			system("pause");
			system("cls");
			continue;
		}
		le.push_back(letra[0]); // coloca a letra no final do array
		system("cls");
		
		x = 0;

		for (i=0 ; i<tam ; i++)
		{
			if (letra[0] == word[i])
			{
				aux[i] = letra[0];
				x++;
			}
		}
		if(x == 0)
		{
			erro--;
		}
	}while(!compara(word, aux, tam) && erro > 0);
	
	if (erro == 0)
	{
		cout << "\n\tNumero maximo de tentarivas foi atingido!\n"
		        "\tA palavra correta era: " << word ;
	}
	
	le.~vector(); //destrutor (causando erro)

	return 0;
}

void exibe (int tam, string d)
{
	system("color 0c");
	cout << "\t     JOOOOOGO DA FORCA\n\n";

	cout << "\t\t ________\n"
	        "\t\t|        |\n"
	        "\t\t|        |\n"
	        "\t\t|\n";
	if(d != " ")
	{
		cout << "\t\t|      DICA: " << d <<"\n"
		        "\t\t|      CHANCES DE ERRO: " << erro << "\n"
		        "\t\t|      LETRAS USADAS: ";
		        for(i=0;i<le.size();i++)
		        {
		        	cout << le.at(i)<< "  ";
				}
				cout << "\n";
	}
	cout<<  "\t\t|\n"
	        "\t\t|\n"
	        "\t\t|\n"
	        "\t\t| ";
	        
	for(i=0;i<tam;i++)
	{
		cout << aux[i] << " ";
    }

}

bool compara(string a, string b, int tam)
{
	int j=0;
	
	for(i=0;i<tam;i++)
	{
		if (a[i] == b[i])
		{
			j++;
		}
	}
	
	if (j == tam)
	{
		system ("color 3F");
		cout << "\n\t Parabens, voce conseguiu !!\n\t PALAVRA = " << a;
		return true;
	}
	
	return false;
}

void converter (string &s)
{
	for(i=0; i<s.size();i++)
	{
		if (s[i] == ' ')
		{
			s[i] = '-';
			aux[i] = '-';
			continue;
		}
		s[i] = toupper(s[i]);
	}
}

bool confere(char l){
	vector<char>::iterator it= le.begin();
	
	while(it != le.end()){
		if(l == *it){ return true;}
		it++;
	}
	
	return false;
}
